package ru.neemble.api.master.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.neemble.api.Application;
import ru.neemble.api.notifications.ImportExportClass;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by gmoiseychenko on 07.09.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class DefaultExportImportMasterTest {

    @Autowired
    DefaultExportImportMaster defaultExportImportMaster;


    @Test
    public void testExport() throws IOException {
        List<UUID> clientIds = new ArrayList<>();
        clientIds.add(UUID.randomUUID());
        clientIds.add(UUID.randomUUID());
        defaultExportImportMaster.exportData(UUID.randomUUID().toString(),UUID.randomUUID(),UUID.randomUUID(),UUID.randomUUID(),clientIds, ImportExportClass.CLIENT);
    }
}
