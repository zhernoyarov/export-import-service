package ru.neemble.api.exportwriter;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import ru.neemble.api.domain.Template;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by gmoiseychenko on 07.09.15.
 */
public abstract class AbstractExportWriter<T> implements ExportWriter{

    private static final int BUFFER = 2048;

    @Value("${output.rootDir}")
    protected String filesOutputDir;

    public void processData(String exportId, UUID companyId, UUID sellPointId, Template template, List<UUID> entityIds) throws IOException {
        String resultFilePath = exportData(exportId, companyId, sellPointId, template, entityIds);
        zipResult(resultFilePath);
        clearTemporaryFiles(resultFilePath);
    };

    private void clearTemporaryFiles(String rootDir) throws IOException {

        File file = new File(rootDir);

        if (getProcessingResult().equals(ProcessingResult.FILE)) {
            file.delete();
        } else {
            FileUtils.deleteDirectory(file);
        }

    }

    protected abstract String exportData(String exportId, UUID companyId, UUID sellPointId, Template template, List<UUID> entityIds) throws IOException;

    public File zipResult(String fileName) throws IOException {
        File rootDir = getProcessingResult().equals(ProcessingResult.FILE) ? new File(filesOutputDir) : new File(fileName);
        File resultFile = new File(fileName + ".zip");
        FileOutputStream dest = new FileOutputStream(resultFile);
        ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));

        BufferedInputStream origin;
        byte data[] = new byte[BUFFER];
        String files[] = getProcessingResult().equals(ProcessingResult.FILE) ? new String[]{Paths.get(fileName).getFileName().toString()} : rootDir.list();
        for (String file : files) {
            FileInputStream fi = new
                    FileInputStream(new File(rootDir, file));
            origin = new
                    BufferedInputStream(fi, BUFFER);
            ZipEntry entry = new ZipEntry(file);
            out.putNextEntry(entry);
            int count;
            while ((count = origin.read(data, 0,
                    BUFFER)) != -1) {
                out.write(data, 0, count);
            }
            origin.close();
        }
        out.close();
        return resultFile;
    }

    protected abstract ProcessingResult getProcessingResult();


    public enum ProcessingResult {
        FILE,FOLDER
    }
}
