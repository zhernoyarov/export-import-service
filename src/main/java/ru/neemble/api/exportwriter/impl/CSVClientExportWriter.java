package ru.neemble.api.exportwriter.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.neemble.api.domain.Client;
import ru.neemble.api.domain.ClientPropertyValue;
import ru.neemble.api.domain.Template;
import ru.neemble.api.exportwriter.AbstractExportWriter;
import ru.neemble.api.exportwriter.ExportWriter;
import ru.neemble.api.repository.ClientRepository;
import ru.neemble.api.repository.TemplateRepository;
import sun.misc.BASE64Decoder;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * Created by gmoiseychenko on 07.09.15.
 */
@Log4j
@Service
public class CSVClientExportWriter extends AbstractExportWriter<Client> {

    @Autowired
    private ClientRepository clientRepository;

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private TemplateRepository templateRepository;

    private static final String NEW_LINE_SEPARATOR = "\n";


    @Override
    protected String exportData(String exportId, UUID companyId, UUID sellPointId, Template template, List<UUID> entityIds) throws IOException {

        List<Client> clients = clientRepository.getClients(companyId, entityIds);

        String fileName = filesOutputDir + "/" +  exportId + ".csv";
        try (FileWriter fileWriter = new FileWriter(fileName)){
            List<String> exportFields = objectMapper.readValue(template.getMappingRule(), List.class);

            String header = getHeader(exportFields, clients.get(0).getProperties(), template.getSeparator());
            fileWriter.append(header);
            fileWriter.append(NEW_LINE_SEPARATOR);

            for (Client client : clients) {
                fileWriter.append(getClientLine(exportFields, client.getProperties(), template.getSeparator()));
                fileWriter.append(NEW_LINE_SEPARATOR);
            }


        return fileName;

        } catch (IOException e) {
            throw e;
        }

    }

    private String getHeader(List<String> systemNames, List<ClientPropertyValue> propertyValues, String separator) {
        StringBuilder columnNames = new StringBuilder();

        for (String systemName : systemNames) {
            for (ClientPropertyValue clientPropertyValue : propertyValues) {
                if (clientPropertyValue.getSystemName().equals(systemName)) {
                    columnNames.append(clientPropertyValue.getName());
                    columnNames.append(separator);
                    break;
                }
            }
        }

        return columnNames.toString().substring(0, columnNames.length() - 2);
    }

    private String getClientLine(List<String> systemNames, List<ClientPropertyValue> propertyValues, String separator) {
        StringBuilder columnNames = new StringBuilder();

        for (String systemName : systemNames) {
            for (ClientPropertyValue clientPropertyValue : propertyValues) {
                if (clientPropertyValue.getSystemName().equals(systemName)) {
                    columnNames.append(clientPropertyValue.getValue());
                    columnNames.append(separator);
                    break;
                }
            }
        }

        return columnNames.toString().substring(0, columnNames.length() - 2);
    }

    @Override
    protected ProcessingResult getProcessingResult() {
        return ProcessingResult.FILE;
    }
}
