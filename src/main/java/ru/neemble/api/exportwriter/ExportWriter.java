package ru.neemble.api.exportwriter;

import ru.neemble.api.domain.Template;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * Created by gmoiseychenko on 07.09.15.
 */
public interface ExportWriter {

    void processData(String exportId, UUID companyId, UUID sellPointId, Template template, List<UUID> entityIds) throws IOException;
}
