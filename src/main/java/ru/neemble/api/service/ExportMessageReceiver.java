package ru.neemble.api.service;

import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalDateTime;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;
import ru.neemble.api.master.ExportImportMaster;
import ru.neemble.api.notifications.*;

import java.io.IOException;

/**
 * Created by arkady on 04.08.15 for neemble.
 */
@Slf4j
@Component
public class ExportMessageReceiver {

    @Value("${export.rabbit.exchange}")
    String exchange;

    @Value("${export.rabbit.queue.results}")
    String resultQueue;


    @Value("${import.rabbit.exchange}")
    String importExchange;

    @Value("${import.rabbit.queue.results}")
    String importResultQueue;

    @Autowired
    MessageConverter messageConverter;

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    ExportImportMaster exportImportMaster;

    @RabbitListener(queues = "${export.rabbit.queue.requests}")
    public void receiveMessage(ExportRequest request) {
        rabbitTemplate.convertAndSend(exchange, resultQueue, messageTemplate(doProcessTask(request)));
    }

    @RabbitListener(queues = "${import.rabbit.queue.requests}")
    public void receiveImportMessage(ImportRequest request) {
        rabbitTemplate.convertAndSend(importExchange, importResultQueue, messageTemplate(doProcessImportTask(request)));
    }


    public Message messageTemplate(Object messageBody) {
        MessageProperties properties = new MessageProperties();
        properties.setContentType("application/json");

        return messageConverter.toMessage(messageBody, properties);
    }


    private ExportResult doProcessTask(ExportRequest request) {
        ExportResult result;
        try {
            exportImportMaster.exportData(request.getExportId().toString(),request.getTemplateId(), request.getCompanyId(), request.getSellPointId(), request.getEntityIds(), request.getImportExportClass());
            result = new ExportResult(request.getExportId(), 0, "completed");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result = new ExportResult(request.getExportId(), 1, e.getMessage());
        }
        return result;
    }

    private ImportResult doProcessImportTask(ImportRequest request) {
        ImportResult result;
        try {
            exportImportMaster.importData(request.getImportId().toString(),request.getTemplateId(), request.getCompanyId(), request.getSellPointId(), request.getImportFilePath(), request.getImportExportClass(),request.getFromLine());
            result = new ImportResult(request.getImportId(), 0, "completed");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result = new ImportResult(request.getImportId(), 1, e.getMessage());
        }
        return result;
    }

}
