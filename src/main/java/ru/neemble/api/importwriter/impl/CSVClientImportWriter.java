package ru.neemble.api.importwriter.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ru.neemble.api.domain.Client;
import ru.neemble.api.domain.ClientProperty;
import ru.neemble.api.domain.ClientPropertyValue;
import ru.neemble.api.domain.Template;
import ru.neemble.api.importwriter.ImportWriter;
import ru.neemble.api.importwriter.util.TokenDto;
import ru.neemble.api.notifications.ImportData;
import ru.neemble.api.notifications.ImportExportClass;
import ru.neemble.api.repository.ClientRepository;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by gmoiseychenko on 12.09.15.
 */
public class CSVClientImportWriter implements ImportWriter {

    @Autowired
    private ClientRepository clientRepository;

    @Value("${import.erp.url.start}")
    private String importStartUrl;

    @Value("${import.erp.url.end}")
    private String importEndUrl;

    @Value("${import.erp.authentication.baseUrl}")
    private String erpBaseUrl;

    @Value("${import.erp.authentication.url}")
    private String erpAuthenticationUrl;

    @Value("${import.erp.authentication.userName}")
    private String erpUserName;

    @Value("${import.erp.authentication.password}")
    private String erpUserPassword;

    @Value("${import.erp.authentication.basicToken}")
    private String erpBasicToken;

    private String erpToken;


    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void processData(String importId, UUID companyId, UUID sellPointId, Template template, String fileName, int fromLine) throws IOException {

        BufferedReader br = new BufferedReader(new FileReader(fileName));
        List<String> clients = new ArrayList<>();
        String clientLine;
        List<String> exportFields = objectMapper.readValue(template.getMappingRule(), List.class);
        UUID projectId = clientRepository.getProjectId(companyId);

        int a=0,b;

        for (int i = 0; i< fromLine; i++) {
            br.readLine();
        }

        while ((clientLine = br.readLine()) != null) {
            Client client = getClient(clientLine, template.getSeparator(), exportFields, projectId);
            clients.add(objectMapper.writeValueAsString(client));
        }

        HttpClient httpClient = HttpClientBuilder.create().build();
        StringBuilder postRequest = new StringBuilder();
        postRequest.append(importStartUrl).append("/").append(companyId.toString()).append(importEndUrl);
        HttpPost post = new HttpPost(postRequest.toString());
        post.addHeader("Content-Type", "application/json");
        ImportData importData = new ImportData();
        importData.setCompanyId(companyId);
        importData.setEntityes(clients);
        importData.setImportExportClass(ImportExportClass.CLIENT);
        importData.setImportId(UUID.fromString(importId));
        importData.setSellPointId(sellPointId);
        String body = objectMapper.writeValueAsString(importData);
        HttpEntity entity = new ByteArrayEntity(body.getBytes());
        post.setEntity(entity);
        if (erpToken == null) {
            getNewToken();
        }
        post.setHeader("Authorization", erpToken);
        HttpResponse response = httpClient.execute(post);



        if (response.getStatusLine().getStatusCode() == 401) {
            getNewToken();
            post.setHeader("Authorization", erpToken);
            response = httpClient.execute(post);
        }

        if (response.getStatusLine().getStatusCode() != 204) {
            throw new IllegalArgumentException("Error while import clients");
        }
    }


    private Client getClient(String importString, String separator, List<String> propertyOrder, UUID projectId) {
        String[] clientPropertyValues = importString.split(separator);

        List<ClientProperty> clientProperties = clientRepository.getPropertiesForSystemNames(propertyOrder, projectId);;

        if (clientProperties.size() != clientPropertyValues.length) {
            throw new IllegalArgumentException("Client parsing exception");
        }

        Client client = new Client();

        List<ClientPropertyValue> newClientPropertyValues = new ArrayList<>();
        client.setId(UUID.randomUUID());
        for (int i=0; i < propertyOrder.size(); i++) {
            ClientPropertyValue clientPropertyValue = new ClientPropertyValue();
            clientPropertyValue.setId(UUID.randomUUID());
            clientPropertyValue.setClientId(client.getId());
            clientPropertyValue.setValue(clientPropertyValues[i]);
            clientPropertyValue.setClientPropertyId(clientProperties.get(i).getId());
            newClientPropertyValues.add(clientPropertyValue);
        }

        client.setProperties(newClientPropertyValues);
        client.setProjectId(projectId);

        return client;
    }


    private void getNewToken() {
        HttpClient httpClient = HttpClientBuilder.create().build();
        String tokenUrl = erpBaseUrl + erpAuthenticationUrl;
        HttpPost httpPost = new HttpPost(tokenUrl);
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("grant_type", "password"));
        params.add(new BasicNameValuePair("username", erpUserName));
        params.add(new BasicNameValuePair("password", erpUserPassword));
        httpPost.addHeader("Authorization", erpBasicToken);
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            String responseString = EntityUtils.toString(entity, "UTF-8");
            TokenDto tokenDto = objectMapper.readValue(responseString, TokenDto.class);
            erpToken = "Bearer " + tokenDto.getAccess_token();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage());
        } catch (ClientProtocolException e) {
            throw new RuntimeException(e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }




}
