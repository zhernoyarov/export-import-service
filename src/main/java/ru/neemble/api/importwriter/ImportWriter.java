package ru.neemble.api.importwriter;

import ru.neemble.api.domain.Template;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by gmoiseychenko on 07.09.15.
 */
public interface ImportWriter {

    void processData(String importId, UUID companyId, UUID sellPointId, Template template, String fileName, int fromLine) throws IOException;
}
