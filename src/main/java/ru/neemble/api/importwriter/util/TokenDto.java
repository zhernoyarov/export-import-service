package ru.neemble.api.importwriter.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by gmoiseychenko on 27.08.15.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TokenDto {

    private String access_token;
    private String token_type;
    private String expires_in;
    private String scope;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String login;

}
