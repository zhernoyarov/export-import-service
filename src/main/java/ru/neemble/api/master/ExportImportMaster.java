package ru.neemble.api.master;

import ru.neemble.api.notifications.ImportExportClass;
import ru.neemble.api.notifications.ImportExportFormat;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * Created by gmoiseychenko on 03.09.15.
 */
public interface ExportImportMaster {

    void exportData(String exportId, UUID templateId, UUID companyId,UUID sellPointId, List<UUID> entityIds, ImportExportClass exportClass) throws IOException;

    void importData(String importId, UUID templateId, UUID companyId, UUID sellPointId, String importFilePath, ImportExportClass importExportClass, int fromLine) throws IOException;

}
