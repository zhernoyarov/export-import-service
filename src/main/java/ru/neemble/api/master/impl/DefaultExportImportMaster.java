package ru.neemble.api.master.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.neemble.api.config.WriterConfiguration;
import ru.neemble.api.domain.Template;
import ru.neemble.api.exportwriter.ExportWriter;
import ru.neemble.api.importwriter.ImportWriter;
import ru.neemble.api.master.ExportImportMaster;
import ru.neemble.api.notifications.ImportExportClass;
import ru.neemble.api.notifications.ImportExportFormat;
import ru.neemble.api.repository.TemplateRepository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by gmoiseychenko on 07.09.15.
 */
@Service
public class DefaultExportImportMaster implements ExportImportMaster {

    @Autowired
    TemplateRepository templateRepository;

    @Autowired
    WriterConfiguration writerConfiguration;

    private static Map<ImportExportClass, Map<ImportExportFormat, ImportWriter>> importWriterStorage = new HashMap<>();

    private static Map<ImportExportClass, Map<ImportExportFormat, ExportWriter>> exportWriterStorage = new HashMap<>();

    @PostConstruct
    private void initWriterStorages() {
        initExportWriterStorage();
        initImportWriterStorage();
    }

    private void initExportWriterStorage(){
        Map<ImportExportFormat, ExportWriter> writers = new HashMap<>();
        writers.put(ImportExportFormat.CSV, writerConfiguration.clientCsvExportWriter());
        exportWriterStorage.put(ImportExportClass.CLIENT, writers);
    }

    private void initImportWriterStorage() {
        Map<ImportExportFormat, ImportWriter> writers = new HashMap<>();
        writers.put(ImportExportFormat.CSV, writerConfiguration.clientCsvImportWriter());
        importWriterStorage.put(ImportExportClass.CLIENT, writers);
    }

    @Override
    public void exportData(String exportId, UUID templateId, UUID companyId, UUID sellPointId, List<UUID> entityIds, ImportExportClass exportClass) throws IOException {
        Template template = templateRepository.getTemplate(templateId);
        ExportWriter exportWriter = exportWriterStorage.get(exportClass).get(template.getFormat());
        exportWriter.processData(exportId, companyId, sellPointId,template, entityIds);
    }

    @Override
    public void importData(String importId, UUID templateId, UUID companyId, UUID sellPointId, String importFilePath, ImportExportClass importClass, int fromLine) throws IOException {
        Template template = templateRepository.getTemplate(templateId);
        ImportWriter importWriter = importWriterStorage.get(importClass).get(template.getFormat());
        importWriter.processData(importId, companyId, sellPointId,template, importFilePath,fromLine);
    }
}
