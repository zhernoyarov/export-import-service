package ru.neemble.api.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.context.ApplicationContextException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arkady on 06.08.15 for neemble.
 */
@Configuration
@Slf4j
public class DatabaseConfiguration {

    @Value("${spring.datasource.databaseName}")
    private String databaseName;

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.dataSourceClassName}")
    private String dataSourceClassName;

    @Value("${spring.datasource.serverName}")
    private String serverName;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${spring.datasource.maximumPoolSize}")
    private Integer maximumPoolSize;

    @Bean(destroyMethod = "shutdown")
    @ConditionalOnMissingClass(name = "ru.neemble.api.config.HerokuDatabaseConfiguration.class")
    public DataSource dataSource() {
//        log.debug("Configuring Datasource");
        if (url == null && databaseName == null) {
//            log.error("Your database connection pool configuration is incorrect! The application" +
//                    "cannot start. Please check your Spring profile");

            throw new ApplicationContextException("Database connection pool is not configured correctly");
        }
        HikariConfig config = new HikariConfig();
        config.setDataSourceClassName(dataSourceClassName);
//        log.debug("Servername: {}, DatabaseName: {}, url: {}", serverName, databaseName, url);
        if (url == null || "".equals(url)) {
            config.addDataSourceProperty("databaseName", databaseName);
            config.addDataSourceProperty("serverName", serverName);
        } else {
            config.addDataSourceProperty("url", url);
        }
        config.addDataSourceProperty("user", username);
        config.addDataSourceProperty("password", password);
        config.setMaximumPoolSize(maximumPoolSize);

        return new HikariDataSource(config);
    }

    @Bean(name = {"org.springframework.boot.autoconfigure.AutoConfigurationUtils.basePackages"})
    public List<String> getBasePackages() {
        List<String> basePackages = new ArrayList<>();
        basePackages.add("ru.neemble.api.domain");
        return basePackages;
    }
}
