package ru.neemble.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.neemble.api.domain.Client;
import ru.neemble.api.exportwriter.ExportWriter;
import ru.neemble.api.exportwriter.impl.CSVClientExportWriter;
import ru.neemble.api.importwriter.ImportWriter;
import ru.neemble.api.importwriter.impl.CSVClientImportWriter;

/**
 * Created by gmoiseychenko on 10.09.15.
 */
@Configuration
public class WriterConfiguration {

    @Bean
    public ExportWriter clientCsvExportWriter(){
        return new CSVClientExportWriter();
    }

    @Bean
    public ImportWriter clientCsvImportWriter(){
        return new CSVClientImportWriter();
    }
}
