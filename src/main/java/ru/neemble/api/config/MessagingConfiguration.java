package ru.neemble.api.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by arkady on 02.07.15 for neemble.
 */
@Slf4j
@Configuration
@EnableRabbit
public class MessagingConfiguration {

    @Value("${export.rabbit.exchange}")
    String exchangeName;

    @Value("${export.rabbit.queue.requests}")
    String taskRequestsQueueName;

    @Value("${export.rabbit.queue.results}")
    String taskResultsQueueName;


    @Value("${import.rabbit.exchange}")
    String importExchangeName;

    @Value("${import.rabbit.queue.requests}")
    String importTaskRequestsQueueName;

    @Value("${import.rabbit.queue.results}")
    String importTaskResultsQueueName;

    @Value("${notifications.rabbit.host}")
    private String host;

    @Value("${notifications.rabbit.user.name}")
    private String username;

    @Value("${notifications.rabbit.user.password}")
    private String password;

    @Bean
    Queue taskRequestsQueue() {
        return new Queue(taskRequestsQueueName, true);
    }

    @Bean
    Exchange tasksExchange() {
        return new TopicExchange(exchangeName, true, true);
    }


    @Bean
    Queue importTaskRequestsQueue() {
        return new Queue(importTaskRequestsQueueName, true);
    }

    @Bean
    Exchange importTasksExchange() {
        return new TopicExchange(importExchangeName, true, true);
    }

    @Bean
    Binding requestsBinding(Queue taskRequestsQueue, TopicExchange tasksExchange) {
        return BindingBuilder.bind(taskRequestsQueue).to(tasksExchange).with(taskRequestsQueueName);
    }


    @Bean
    Binding importRequestsBinding(Queue importTaskRequestsQueue, TopicExchange importTasksExchange) {
        return BindingBuilder.bind(importTaskRequestsQueue).to(importTasksExchange).with(importTaskRequestsQueueName);
    }

    @Bean
    public MessageConverter messageConverter() {
        Jackson2JsonMessageConverter messageConverter = new Jackson2JsonMessageConverter();
        messageConverter.setJsonObjectMapper(new ObjectMapper());
        return messageConverter;
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory cf = new CachingConnectionFactory(host);
        cf.setUsername(username);
        cf.setPassword(password);
        return cf;
    }

    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(ConnectionFactory connectionFactory, MessageConverter messageConverter) {
        log.info("Configure container factory");
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setMessageConverter(messageConverter);
        factory.setConnectionFactory(connectionFactory);
        log.info("Done: configure container factory");
        return factory;
    }


    @Bean
    public MessageConverter importMessageConverter() {
        Jackson2JsonMessageConverter messageConverter = new Jackson2JsonMessageConverter();
        messageConverter.setJsonObjectMapper(new ObjectMapper());
        return messageConverter;
    }

    @Bean
    public ConnectionFactory importConnectionFactory() {
        CachingConnectionFactory cf = new CachingConnectionFactory(host);
        cf.setUsername(username);
        cf.setPassword(password);
        return cf;
    }

    @Bean
    public SimpleRabbitListenerContainerFactory importRabbitListenerContainerFactory(ConnectionFactory importConnectionFactory, MessageConverter importMessageConverter) {
        log.info("Configure container factory");
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setMessageConverter(importMessageConverter);
        factory.setConnectionFactory(importConnectionFactory);
        log.info("Done: configure container factory");
        return factory;
    }

}
