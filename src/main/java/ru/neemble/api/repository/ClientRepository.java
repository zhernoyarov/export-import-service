package ru.neemble.api.repository;

import com.google.common.collect.ImmutableMap;
import org.apache.tomcat.util.codec.binary.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Service;
import ru.neemble.api.domain.Client;
import ru.neemble.api.domain.ClientProperty;
import ru.neemble.api.domain.ClientPropertyValue;

import javax.sql.DataSource;
import java.util.*;

/**
 * Created by gmoiseychenko on 07.09.15.
 */
@Service
public class ClientRepository extends NamedParameterJdbcDaoSupport {

    @Autowired
    ClientRepository(DataSource dataSource) {
        setDataSource(dataSource);
    }


    String startGetClientsSql = "select cpv.client_id,cpv.value, cp.system_name, cp.name from t_client_property_value as cpv left join t_client_property as cp on (cpv.client_property_id = cp.id) " +
            " where cpv.client_id in ";

    String endGetClientsSql = " order by cpv.client_id";

    String getClientPropertiesSql = "select cp.id, cp.system_name from t_client_property as cp left join t_property_group as pg on (pg.id = cp.property_group_id) where " +
            "pg.project_id = cast(:projectId as UUID)";

    String getProjectSql = "select id from t_project where company_id = cast(:companyId as uuid)";

    String insertClientSql = "insert into t_client (id,project_id, created_date) values (cast(:id as uuid), " +
            "cast(:projectId as uuid), cast(:createdDate as timestamp without time zone))";

    String insertClientPropertyValueSql = "insert into t_client_property_value(id,client_id, value, client_property_id)" +
            " values(cast(:id as uuid),cast(:clientId as uuid), :value, cast(:clientPropertyId as uuid))";


    public List<Client> getClients(UUID companyId, List<UUID> clientIds) {
        List<Client> result = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append(startGetClientsSql);
        sb.append("(");

        for (UUID clientId : clientIds) {
            sb.append("cast('").append(clientId.toString()).append("' as UUID),");
        }
        sb.replace(sb.length() - 1, sb.length(), ")");

        sb.append(endGetClientsSql);

        List<ClientPropertyValue> clientPropertyValues = getNamedParameterJdbcTemplate().query(
                sb.toString(),
//                ImmutableMap.<String, Object>builder().put("clientIds", sb.toString()).build(),
                (rs, rowNum) -> {
                    ClientPropertyValue clientPropertyValue = new ClientPropertyValue();
                    clientPropertyValue.setClientId(UUID.fromString(rs.getString("client_id")));
                    clientPropertyValue.setValue(rs.getString("value"));
                    clientPropertyValue.setSystemName(rs.getString("system_name"));
                    clientPropertyValue.setName(rs.getString("name"));
                    return clientPropertyValue;
                });

        if (clientPropertyValues.size() > 0) {
            Client client = new Client();
            client.setId( clientPropertyValues.get(0).getClientId());
            result.add(client);
            for (ClientPropertyValue clientPropertyValue : clientPropertyValues) {
                if (!clientPropertyValue.getClientId().equals(client.getId())) {
                    client = new Client();
                    result.add(client);
                }
                client.getProperties().add(clientPropertyValue);
            }
        }



        return result;
    }

    public UUID getProjectId(UUID companyId) {

        UUID projectId = getNamedParameterJdbcTemplate().queryForObject(getProjectSql,
                ImmutableMap.<String, Object>builder().put("companyId", companyId.toString()).build(),
                (rs, rowNum) -> {
                    return UUID.fromString(rs.getString("id"));
                });

        return projectId;
    }

    public void saveClients(List<Client> clients) {

        List<Map<String, String>> clientsUpdateMap = new ArrayList<>();
        List<Map<String, String>> clientPropsUpdateMap = new ArrayList<>();

        for (Client client : clients) {
            Map<String, String> clientMap = new HashMap<>();
            clientMap.put("id", client.getId().toString());
            clientMap.put("projectId", client.getProjectId().toString());
            clientMap.put("createdDate", DateTime.now().toString("YYYY-MM-dd"));
            clientsUpdateMap.add(clientMap);
            for (ClientPropertyValue clientPropertyValue : client.getProperties()) {
                Map<String, String> propertyMap = new HashMap<>();
                propertyMap.put("id", clientPropertyValue.getId().toString());
                propertyMap.put("clientId", client.getId().toString());
                propertyMap.put("value", clientPropertyValue.getValue());
                propertyMap.put("clientPropertyId", clientPropertyValue.getClientPropertyId().toString());
                clientPropsUpdateMap.add(propertyMap);
            }
        }

        getNamedParameterJdbcTemplate().batchUpdate(insertClientSql, (Map<String, String>[]) clientsUpdateMap.toArray());

        getNamedParameterJdbcTemplate().batchUpdate(insertClientPropertyValueSql, (Map<String, String>[]) clientPropsUpdateMap.toArray());
    }

    public List<ClientProperty> getPropertiesForSystemNames(List<String> propertyOrder, UUID projectId) {

        List<ClientProperty> clientProperties = getNamedParameterJdbcTemplate().query(
                getClientPropertiesSql,
                ImmutableMap.<String, Object>builder().put("projectId", projectId.toString()).build(),
                (rs, rowNum) -> {
                    ClientProperty clientProperty = new ClientProperty();
                    clientProperty.setId(UUID.fromString(rs.getString("id")));
                    clientProperty.setSystemName(rs.getString("system_name"));
                    return clientProperty;
                });

        List<ClientProperty> result = new ArrayList<>();

        for (String propertyInOrder : propertyOrder) {
            for (ClientProperty property : clientProperties) {
                if (property.getSystemName().equals(propertyInOrder)) {
                    result.add(property);
                    break;
                }
            }
        }

        return result;
    }
}
