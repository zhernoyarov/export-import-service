package ru.neemble.api.repository;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Service;
import ru.neemble.api.domain.ClientPropertyValue;
import ru.neemble.api.domain.Template;
import ru.neemble.api.notifications.ImportExportFormat;

import javax.sql.DataSource;
import java.util.List;
import java.util.UUID;

/**
 * Created by gmoiseychenko on 07.09.15.
 */
@Service
public class TemplateRepository extends NamedParameterJdbcDaoSupport {

    @Autowired
    TemplateRepository(DataSource dataSource) {
        setDataSource(dataSource);
    }

    String sql = "select mapping_rule, separator, template_type, format from t_export_import_template where id = cast(:id as UUID) limit 1";

    public Template getTemplate(UUID templateId) {
        List<Template> templates = getNamedParameterJdbcTemplate().query(
                sql,
                ImmutableMap.<String, Object>builder().put("id", templateId.toString()).build(),
                (rs, rowNum) -> {
                    Template template = new Template();
                    template.setMappingRule(rs.getString("mapping_rule"));
                    template.setSeparator(rs.getString("separator"));
                    template.setFormat(ImportExportFormat.valueOf(rs.getString("format")));
                    return template;
                });

        return (templates.size() == 1) ? templates.get(0) : null;
    }
}
