package ru.neemble.api.domain;

import lombok.Data;

import java.util.UUID;

/**
 * Created by gmoiseychenko on 12.09.15.
 */
@Data
public class ClientProperty {

    private UUID id;

    private String systemName;
}
