package ru.neemble.api.domain;

import lombok.Data;

import java.util.UUID;

/**
 * Created by gmoiseychenko on 07.09.15.
 */
@Data
public class ClientPropertyValue {

    private String systemName;

    private String value;

    private UUID clientId;

    private String name;

    private UUID clientPropertyId;

    private UUID id;
}
