package ru.neemble.api.domain;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by gmoiseychenko on 07.09.15.
 */
@Data
public class Client {

    private UUID id;

    private UUID projectId;

    private List<ClientPropertyValue> properties = new ArrayList<>();
}
