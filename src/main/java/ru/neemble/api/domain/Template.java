package ru.neemble.api.domain;

import lombok.Data;
import ru.neemble.api.notifications.ImportExportFormat;

/**
 * Created by gmoiseychenko on 07.09.15.
 */
@Data
public class Template {

    String mappingRule;

    String separator;

    ImportExportFormat format;

}
